
#pragma once


#include <QQuickItem>


class RotationItem : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QString description READ description NOTIFY descriptionChanged)

public:
	explicit RotationItem(QQuickItem *parent = 0);
	~RotationItem();

public:
    QString description() const;

signals:
    void descriptionChanged(QString & description);

public slots:
    void test() {
      qDebug("Test!");
    }
};


