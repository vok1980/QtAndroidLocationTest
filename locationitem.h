
#pragma once


#include <QQuickItem>

//class QGeoPositionInfoSource;
#include <QGeoPositionInfoSource>


class LocationItem : public QQuickItem
{
    Q_OBJECT
	Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)
	Q_PROPERTY(bool started READ started WRITE setStarted NOTIFY startedChanged)
	Q_PROPERTY(QString method READ method WRITE setMethod)

public:
    explicit LocationItem(QQuickItem *parent = 0);
    ~LocationItem();

public:
	void setDescription(QString);
    QString description() const;
	void setStarted(bool);
	bool started() const;
	void setMethod(const QString &);
	QString method() const;

signals:
    void descriptionChanged(QString & description);
	void startedChanged(bool started);
	void buttonStateChanged(bool enabled);

public slots:
	void onError(QGeoPositionInfoSource::Error);
	void onPositionUpdated(const QGeoPositionInfo &);
	void onUpdateTimeout();
	void onApplicationActivityChanged(Qt::ApplicationState state);

	void test() {
      qDebug("Test!");
    }

private:
	void changeSourceState();

private:
	QGeoPositionInfoSource *source_;
	bool started_;
	QString description_;
	QString method_;
};


