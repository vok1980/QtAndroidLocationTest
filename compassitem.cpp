#include "compassitem.h"

#include <cassert>
#include <Qt>
#include <QApplication>
#include <QCompass>


#ifdef Q_OS_ANDROID
#include "QAndroidCompass.h"
#endif


#define ABSf(x) (((x) >= 0.f) ? (x) : -(x))

namespace {
	static int a =
			qmlRegisterType<CompassItem>("ru.voksoft.CompassItem", 1, 0, "CompassItem");
}


CompassItem::CompassItem() :
	m_azimutCustom(0.f)
	, m_azimutOrig(0.f)
{
	m_compassOrig = new QCompass(this);
	QObject::connect(m_compassOrig, SIGNAL(readingChanged()),
					 this, SLOT(onCompassChanged()));


#ifdef Q_OS_ANDROID
	m_compassCustom = new QAndroidCompass(this);
	QObject::connect(m_compassCustom, SIGNAL(azimutChanged(float)),
					 this, SLOT(onCustomCompassChanged(float)));
#endif ///Q_OS_ANDROID

	QObject::connect(
			QApplication::instance(), SIGNAL(applicationStateChanged(Qt::ApplicationState)),
			this,					SLOT(onApplicationActivityChanged(Qt::ApplicationState)));

	onApplicationActivityChanged(QApplication::applicationState());
}


CompassItem::~CompassItem()
{
	if (m_compassOrig)
		m_compassOrig->stop();

#ifdef Q_OS_ANDROID
	if (m_compassCustom)
		m_compassCustom->stop();
#endif
}


void CompassItem::onApplicationActivityChanged(Qt::ApplicationState state)
{
	if (Qt::ApplicationActive == state)
	{
		if (m_compassOrig)
			m_compassOrig->start();

#ifdef Q_OS_ANDROID
		if (m_compassCustom)
			m_compassCustom->start();
#endif
	}
	else
	{
		if (m_compassOrig)
			m_compassOrig->stop();

#ifdef Q_OS_ANDROID
		if (m_compassCustom)
			m_compassCustom->stop();
#endif
	}
}


QString CompassItem::azimutCustom()
{
	return QString::number(m_azimutCustom);
}


QString CompassItem::azimutOrig()
{
	return QString::number(m_azimutOrig);
}


void CompassItem::onCustomCompassChanged(float azimut)
{

	{
		m_azimutCustom = azimut;
		emit azimutCustomChanged();
	}
}


void CompassItem::onCompassChanged()
{
	assert(m_compassOrig);
	QCompassReading * data = m_compassOrig->reading();

	assert(data);

	if (data)
	{
		qWarning() << __FUNCTION__ << ":azimut=" << data->azimuth();

		if (ABSf(data->azimuth() - m_azimutOrig) > 1.f)
		{
			qWarning() << __FUNCTION__ << ":azimut=" << data->azimuth();
			m_azimutOrig = data->azimuth();
			emit azimutOrigChanged();
		}
	}
}


