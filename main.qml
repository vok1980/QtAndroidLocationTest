import QtQuick 2.1
import QtQuick.Controls 1.0

import ru.voksoft.LocationItem 1.0;
import ru.voksoft.CompassItem 1.0;



ApplicationWindow
{
    title: "test window"
    id: mainWindow
    width: 320
    height: 480
    visible: true

    CompassItem {
        id: compassItemId;
        height: 10;
        anchors.top: parent.top;

        Text {
            id: origAz;
            text: compassItemId.azimutOrig;
            font.family: "Helvetica"
            font.pointSize: 12
            color : "blue";
        }

        Text {
            anchors.left:  origAz.right
            anchors.top:  origAz.bottom;
            text: compassItemId.azimutCustom;
            font.family: "Helvetica"
            font.pointSize: 12
            color : "green";
        }

    }

    LocationItem {
        id : locationitem00;
        anchors.centerIn: parent;
        width : parent.width;
//        height: parent.height;
        anchors.top: compassItemId.bottom;
        anchors.bottom: parent.bottom;
        started : start_button.checked
        method: pos_method.currentText;

        onButtonStateChanged: {
            start_button.checked = started;
        }


        ComboBox {
            id: pos_method;
            width: parent.width / 2;
            anchors.top: parent.top;
            anchors.horizontalCenter: parent.horizontalCenter;

            model: ["AllPositioningMethods",
                    "SatellitePositioningMethods",
                    "NonSatellitePositioningMethods",
                    "NoPositioningMethods"]
        }

        Button {
            anchors.horizontalCenter: parent.horizontalCenter;
            anchors.top: pos_method.bottom;

            id: start_button;
            text: checked ? "Stop" : "Start";
            checkable: true;
            width: parent.width / 2;
        }

        Text {
            text: locationitem00.description;
            font.family: "Helvetica"
            font.pointSize: 12
            color : "red";
            anchors.top: start_button.bottom;
            anchors.bottom: button_clear.top;
        }

        Button {
            id: button_clear;
            text: "Clear"
            anchors.bottom: parent.bottom;

            onClicked: {
                locationitem00.description = "";
            }
        }

    }
}

