#ifndef COMPASSITEM_H
#define COMPASSITEM_H

#include <QQuickItem>
#include <QCompass>
#include <QPointer>

#ifdef Q_OS_ANDROID
#include "QAndroidCompass.h"
#endif



class CompassItem : public QQuickItem
{
	Q_OBJECT
	Q_PROPERTY(QString azimutOrig READ azimutOrig NOTIFY azimutOrigChanged)
	Q_PROPERTY(QString azimutCustom READ azimutCustom NOTIFY azimutCustomChanged)

public:
	CompassItem();
	~CompassItem();

public:
	QString azimutCustom();
	QString azimutOrig();

signals:
	void azimutOrigChanged();
	void azimutCustomChanged();

private slots:
	void onCompassChanged();
	void onCustomCompassChanged(float azimut);
	void onApplicationActivityChanged(Qt::ApplicationState state);

private:
	float m_azimutCustom;
	float m_azimutOrig;

	QPointer<QCompass> m_compassOrig;

#ifdef Q_OS_ANDROID
	QPointer<QAndroidCompass> m_compassCustom;
#endif
};

#endif // COMPASSITEM_H
