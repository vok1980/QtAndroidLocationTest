#include "locationitem.h"

#include <Qt>
#include <QApplication>
#include <QGeoPositionInfoSource>

#ifdef Q_OS_ANDROID
#include "QGeoPositionInfoSourceAndroidGPS.h"
#endif


namespace {
    static int a =
            qmlRegisterType<LocationItem>("ru.voksoft.LocationItem", 1, 0, "LocationItem");
}


LocationItem::LocationItem(QQuickItem *parent) :
    QQuickItem(parent)
	, source_(NULL)
	, started_(false)
{
	qRegisterMetaType<QGeoPositionInfo>("QGeoPositionInfo");

#ifdef Q_OS_ANDROID
	if (QGeoPositionInfoSourceAndroidGPS::isAvailable())
	{
		qDebug() << "creating QGeoPositionInfoSourceAndroidGPS";
		source_ = new QGeoPositionInfoSourceAndroidGPS(this);
	}
#endif

	if (!source_)
	{
		qDebug() << "creating QGeoPositionInfoSource::createDefaultSource";
		source_ = QGeoPositionInfoSource::createDefaultSource(this);
	}

	QObject::connect(
			QApplication::instance(), SIGNAL(applicationStateChanged(Qt::ApplicationState)),
			this,					SLOT(onApplicationActivityChanged(Qt::ApplicationState)));

	if (source_)
	{
		qDebug() << "QGeoPositionInfoSource source " << source_->sourceName() << " created";
		description_ = "QGeoPositionInfoSource source \"" + source_->sourceName() + "\" created\n";

		QObject::connect(source_, SIGNAL(error(QGeoPositionInfoSource::Error)),
						 this, SLOT(onError(QGeoPositionInfoSource::Error)));

		QObject::connect(source_, SIGNAL(positionUpdated(const QGeoPositionInfo &)),
						 this, SLOT(onPositionUpdated(const QGeoPositionInfo &)));

		QObject::connect(source_, SIGNAL(updateTimeout()),
						 this, SLOT(onUpdateTimeout()));

		qDebug() << "min update interval = " << source_->minimumUpdateInterval();
		source_->setUpdateInterval(1000);

		source_->setPreferredPositioningMethods( QGeoPositionInfoSource::AllPositioningMethods );
//		source_->setPreferredPositioningMethods( QGeoPositionInfoSource::NonSatellitePositioningMethods );
//		source_->setPreferredPositioningMethods( QGeoPositionInfoSource::SatellitePositioningMethods );
//		source_->setPreferredPositioningMethods( QGeoPositionInfoSource::NoPositioningMethods );
	}
	else
	{
		qCritical() << "Failed to create QGeoPositionInfoSource";
	}


}


LocationItem::~LocationItem()
{

}


void LocationItem::onError(QGeoPositionInfoSource::Error error)
{
	qDebug() << __FUNCTION__;
	qDebug() << error;

	description_ = "Error: " + QString::number((int)error) + "\n";
	descriptionChanged(description_);
}


void LocationItem::onPositionUpdated(const QGeoPositionInfo &info)
{
	qDebug() << __FUNCTION__;

	description_ += info.isValid() ? "Valid" : "Not valid";
	description_ += " | ";
	description_ += info.timestamp().time().toString();
	description_ += " | ";
	description_ += QString::number(info.coordinate().latitude());
	description_ += " | ";
	description_ += QString::number(info.coordinate().longitude());

	if (info.hasAttribute(QGeoPositionInfo::HorizontalAccuracy))
	{
		description_ += " | ";
		description_ += "acc=" + QString::number(info.attribute(QGeoPositionInfo::HorizontalAccuracy));
	}

	description_ += "\n";

	emit descriptionChanged(description_);

}


void LocationItem::onUpdateTimeout()
{
	qDebug() << __FUNCTION__;

	description_ += "onUpdateTimeout\n";
	descriptionChanged(description_);

}


void LocationItem::setMethod(const QString &method)
{
	method_ = method;

	if (!source_)
		return ;

	if (method == "AllPositioningMethods")
		source_->setPreferredPositioningMethods( QGeoPositionInfoSource::AllPositioningMethods );

	if (method == "NonSatellitePositioningMethods")
		source_->setPreferredPositioningMethods( QGeoPositionInfoSource::NonSatellitePositioningMethods );

	if (method == "SatellitePositioningMethods")
		source_->setPreferredPositioningMethods( QGeoPositionInfoSource::SatellitePositioningMethods );

	if (method == "NoPositioningMethods")
		source_->setPreferredPositioningMethods( QGeoPositionInfoSource::NoPositioningMethods );

}


void LocationItem::onApplicationActivityChanged(Qt::ApplicationState state)
{
	Q_UNUSED(state)
	changeSourceState();
}


QString LocationItem::method() const
{
	return method_;
}


void LocationItem::setStarted(bool started)
{
	started_ = started;
	changeSourceState();
}


void LocationItem::changeSourceState()
{
	Qt::ApplicationState state = QApplication::applicationState();

	if (source_)
	{
		if (started() && (Qt::ApplicationActive == state))
		{
			source_->startUpdates();
			qDebug() << "startUpdates";
		}
		else
		{
			source_->stopUpdates();
			qDebug() << "stopUpdates";
		}
	}
}


bool LocationItem::started() const
{
	return started_;
}


void LocationItem::setDescription(QString description)
{
	description_ = description;
	emit descriptionChanged(description_);
}


QString LocationItem::description() const
{
	return description_;
}

#include "moc_locationitem.cpp"
