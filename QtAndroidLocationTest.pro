DISTFILES += \
    main.qml


SOURCES += \
    main.cpp \
    locationitem.cpp    \
    rotationitem.cpp \
    compassitem.cpp

QT += quick core gui positioning widgets sensors

RESOURCES += \
    resource.qrc

HEADERS += \
    locationitem.h  \
    rotationitem.h \
    compassitem.h

include(ext/qtandroidextensions/QJniHelpers/QJniHelpers.pri)
include(ext/qtandroidextensions/QtAndroidHelpers/QtAndroidHelpers.pri)
include(ext/qtandroidextensions/QtAndroidGpsLocation/QtAndroidGpsLocation.pri)
include(ext/qtandroidextensions/QtAndroidCompass/QtAndroidCompass.pri)

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android-sources
